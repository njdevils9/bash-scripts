#!/bin/bash

read -p 'Type AWS profile: ' AWS_PROFILE
read -p 'Type AWS region: ' REGION

touch /tmp/ebs_vols.txt

aws ec2 describe-volumes --filters Name=status,Values=available --profile=${AWS_PROFILE} --region=${REGION} | grep 'VolumeId' | awk '{print $2}'|cut -d '"' -f2 > /tmp/ebs_vols.txt

for i in `cat /tmp/ebs_vols.txt`; do 
   aws ec2 delete-volume --volume-id $i --profile=${AWS_PROFILE} --region=${REGION}
done

#echo > /tmp/ebs_vols.txt

#for OUTPUT in $(cat .aws/credentials | grep -i '\[*\]'| sed 's/[^A-Za-z_]//g'); do echo $OUTPUT; aws ec2 --profile $OUTPUT --region ${REGION} describe-volumes --filters Name=status,Values=available | grep -e "VolumeId" -e "Name" -e "Value" | grep -v "Name"; done >> unused_volumes.txt
